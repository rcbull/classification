import re
import nltk
from nltk.corpus import stopwords
from bs4 import BeautifulSoup
from nltk.tokenize import word_tokenize
from nltk import ngrams

class CleanText(object):

    def remove_characters_specials(self, file, point=True):
        if point:
            file = re.sub('[^a-zA-Z0-9_.,()-áéíóúâêîôûãõàèìòùç]+', ' ', file)
        else:
            file = re.sub('\W+', ' ', file)

        return file

    def remove_stop_words(self, file):
        stop_words = set(stopwords.words('portuguese'))

        word_tokens = self.get_token(file)

        filtered_sentence = [w for w in word_tokens if not w in stop_words]

        return ' '.join(filtered_sentence)

    def get_token(self, file):
        return word_tokenize(file)

    def stemm_text(self, file):
        stemmer = nltk.stem.RSLPStemmer()

        word_tokens = self.get_token(file)

        stemm_sentence = [stemmer.stem(w) for w in word_tokens]

        return ' '.join(stemm_sentence)

    def remove_html_tags(self, file):
        file_right = BeautifulSoup(file, "html.parser").prettify()
        soup = BeautifulSoup(file_right, "lxml")
        [x.extract() for x in soup.findAll(['script', 'style', 'footer', 'header', 'head'])]

        file = soup.get_text().replace('\\n', ' ').replace('\n', ' ').replace('\t', '').replace('\\t', '')
        return file

    def get_ngram(self, file, n):
        tokens = [token for token in file.split(" ") if token != ""]
        output = list(ngrams(tokens, n))

        return output

    def master_clean(self, file):
        file = self.remove_html_tags(file)
        file = self.remove_characters_specials(file, point=False)
        file = self.remove_stop_words(file)
        file = self.stemm_text(file)

        return file
