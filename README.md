# Teste classificador de notícias

Este projeto foi desenvolvido para testar performance assertividade do classificador de notícias.
Siga as instruções atentamente para obter o resultado esperado.

## "Getting Started"

As próximas instruções o guiarão para instalar dependências para execução do código em python tanto em ambiente Linux como em Windows.

### Pré-requisitos

Para execução do projeto, você precisará ter instalado:
* Python3.5+
* Pip3.5+
* Dependências do requirements.txt

### Installing

#### Python
Certifique-se de que possui o Python 3.5+ instalado de forma correta, conforme link abaixo:

```
https://python.org.br/
```

Teste se o Python está de fato instalado em sua máquina, utilizando o seguinte comando no console:

```
python --version
```
ou em alguns casos pode vir o comando com versão, como exemplo:
```
python3.5 --version
```

#### Pip
Certifique-se de que possui o Pip3.5+ instalado de forma correta, conforme link abaixo:

```
https://pip.pypa.io/en/stable/installing/#
```
Teste se o Pip está de fato instalado em sua máquina, utilizando o seguinte comando no console:

```
pip --version
```
ou em alguns casos pode vir o comando com versão, como exemplo:
```
pip3.5 --version
```

#### Dependências do projeto
Instale as depêndencias do projeto executando o seguinte comando em seu console:

```
pip install -r requirements.txt
```

## Executando o projeto

Para testar se um arquivo é ou não da categoria "POLÍTICA", você precisará:
* Separar o arquivo no formato PDF ou HTML
* Este arquivo deverá ser da fonte Estadão ou Folha de SP para este modelo estatístico
* Pegar o caminho completo onde o arquivo está salvo assim como seu nome com extensão

Para iniciar o código execute o seguinte comando em seu terminal:
```
python test_classification.py
```

O projeto perguntará primeiro o caminho completo do arquivo no seguinte padrão:
```
F:/Users/sug5824/lick_git/classification
```
Ou seja, com barras "/". Pressione ENTER e o processo fará a solicitação do nome do arquivo com sua extensão, conforme exemplo a seguir:

```
ESP-2009-01-000836.pdf
```
Logo em seguida, o processo retornará a classificação do arquivo e a precisão em %.

Um exemplo de execução:

```
Caminho da pasta (separado por /):
F:/Users/sug5824/Documents/PROJETOS/UNESP/positivo

Nome do arquivo com extensão (.pdf/.html)
ESP-2009-01-000836.pdf

Testando arquivo F:/Users/sug5824/Documents/PROJETOS/UNESP/positivo/ESP-2009-01-000836.pdf

Iniciando conversão e limpeza do arquivo

Arquivo enviado é de POLITICA
Precisão de 72.14285714285714%
```
